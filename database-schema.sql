DROP TABLE IF EXISTS oauth.clients CASCADE;

CREATE TABLE oauth.clients (
  id text NOT NULL,
  secret text NOT NULL,
  redirect_uri text,
  PRIMARY KEY (id, secret),
  UNIQUE (id)
) WITH (OIDS = FALSE);

ALTER TABLE oauth.clients OWNER to wrkforce;

DROP TABLE IF EXISTS oauth.users CASCADE;

CREATE TABLE oauth.users (
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  name text NOT NULL,
  password text NOT NULL,
  client_id text NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_users_client FOREIGN KEY (client_id) REFERENCES oauth.clients (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE ONLY oauth.users OWNER to wrkforce;

CREATE INDEX users_name_password ON oauth.users USING btree (name, password);

DROP TABLE IF EXISTS oauth.tokens CASCADE;

CREATE TABLE oauth.tokens (
  id uuid NOT NULL DEFAULT uuid_generate_v4(),
  access_token text NOT NULL,
  access_token_expires_at timestamp without time zone NOT NULL,
  refresh_token text NOT NULL,
  refresh_token_expires_at timestamp without time zone NOT NULL,
  client_id text NOT NULL,
  user_id uuid NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_tokens_client FOREIGN KEY (client_id) REFERENCES oauth.clients (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT fk_tokens_user FOREIGN KEY (user_id) REFERENCES oauth.users (id) MATCH SIMPLE ON UPDATE CASCADE ON DELETE CASCADE
);

ALTER TABLE ONLY oauth.tokens OWNER to wrkforce;

CREATE INDEX users_full_index ON oauth.users USING btree (name text_pattern_ops ASC NULLS LAST, password text_pattern_ops ASC NULLS LAST, client_id text_pattern_ops ASC NULLS LAST) TABLESPACE pg_default;