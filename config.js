module.exports = {
  port: 3000,
  database: {
    host: 'clbr.cl',
    port: 5432,
    database: 'wrkforce-dev',
    user: 'wrkforce',
    password: 'WrkForcePass1'
  },
  accessTokenLifetime: 300,
  refreshTokenLifetime: 600
};