(function () {

'use strict';

const config = require('./config');

const bodyParser = require('body-parser');
//const cookieParser = require('cookie-parser');
const cors = require('cors');
const debug = require('debug');
const express = require('express');
const expressOauthServer = require('express-oauth-server');
const favicon = require('serve-favicon');
const helmet = require('helmet');
const morgan = require('morgan');
const oAuth2Server = require('oauth2-server');
const path = require('path');

let app = express();

app.oauth = new expressOauthServer({
  debug: true,
  model: require('./oauth-model'),
  allowBearerTokensInQueryString: true,
  //useErrorHandler: true,
  accessTokenLifetime: config.accessTokenLifetime,
  refreshTokenLifetime: config.refreshTokenLifetime
});

app
.set('views', path.join(__dirname, 'views'))
.set('view engine', 'ejs');

app
.use(bodyParser.json())
.use(bodyParser.urlencoded({extended: false}))
//.use(cookieParser())
.use(cors())
.use(express.static(path.join(__dirname, 'public')))
.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
.use(helmet())
.use(morgan('dev'));

app.post('/oauth/token', app.oauth.token());

app.use('/login', require('./routes/login'));

app.get('/secret', app.oauth.authenticate(), (req, res) => {
  res.send('Secret area');
});

app.get('/public', (req, res) => {
  res.send('Public area');
});

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  debug('wrkforce:server_error')(err.status, err.message);
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.send(err.status + '<br>' + err.message);
});

app.listen(config.port);

})();