--DELETE FROM oauth.clients;
INSERT INTO oauth.clients (id, secret) VALUES
('client1', 'client1'),
('client2', 'client2'),
('client3', 'client3');

--DELETE FROM oauth.users;
INSERT INTO oauth.users (name, password, client_id) VALUES
('user1', 'user1', 'client1'),
('user2', 'user2', 'client2'),
('user3', 'user3', 'client3');