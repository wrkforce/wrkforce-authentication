const config = require('./config');

const debug = require('debug')('wrkforce:oauth');
const pgp = require('pg-promise')();

let pg = pgp(config.database);

let tempClientId = null;

module.exports.getAccessToken = function(bearerToken) {
  debug('getAccessToken', bearerToken);
  return pg.query('SELECT * FROM oauth.tokens t INNER JOIN oauth.users u ON u.id = t.user_id INNER JOIN oauth.clients c ON c.id = t.client_id WHERE access_token = $1', [bearerToken]).then((result) => {
    if (!result.length) return false;
    let token = result[0];
    return {
      accessToken: token.access_token,
      accessTokenExpiresAt: token.access_token_expires_at,
      refreshToken: token.refresh_token,
      refreshTokenExpiresAt: token.refresh_token_expires_at,
      client: {id: token.client_id},
      user: {id: token.user_id}
    };
  }).catch((err) => {
    debug('getAccessToken - err', err);
    return false;
  });
};

module.exports.getClient = function *(clientId, clientSecret) {
  debug('getClient', clientId, clientSecret);
  return pg.query('SELECT * FROM oauth.clients WHERE id = $1 AND secret = $2', [clientId, clientSecret]).then((result) => {
    if (!result.length) return false;
    let client = result[0];
    tempClientId = client.id;
    return {
      id: client.id,
      secret: client.secret,
      grants: ['password', 'refresh_token'],
    };
  }).catch((err) => {
    debug('getClient - err', err);
    return false;
  });
};

module.exports.getRefreshToken = function *(bearerToken) {
  debug('getRefreshToken', bearerToken);
  return pg.query('SELECT * FROM oauth.tokens t INNER JOIN oauth.users u ON u.id = t.user_id INNER JOIN oauth.clients c ON c.id = t.client_id WHERE refresh_token = $1', [bearerToken]).then((result) => {
    if (!result.length) return false;
    let token = result[0];
    return {
      accessToken: token.access_token,
      accessTokenExpiresAt: token.access_token_expires_at,
      refreshToken: token.refresh_token,
      refreshTokenExpiresAt: token.refresh_token_expires_at,
      client: {id: token.client_id},
      user: {id: token.user_id}
    };
  }).catch((err) => {
    debug('getRefreshToken - err', err);
    return false;
  });
};

module.exports.getUser = function *(name, password) {
  debug('getUser', name, password);
  return pg.query('SELECT * FROM oauth.users WHERE name = $1 AND password = $2 AND client_id = $3', [name, password, tempClientId]).then((result) => {
    if (!result.length) return false;
    return result[0];
  }).catch((err) => {
    debug('getUser - err', err);
    return false;
  });
};

module.exports.saveToken = function *(token, client, user) {
  debug('saveToken', token, client, user);
  if (user.client_id !== client.id) return false;
  let data = [
    token.accessToken,
    token.accessTokenExpiresAt,
    token.refreshToken,
    token.refreshTokenExpiresAt,
    client.id,
    user.id
  ];
  return pg.none('INSERT INTO oauth.tokens (access_token, access_token_expires_at, refresh_token, refresh_token_expires_at, client_id, user_id) VALUES ($1, $2, $3, $4, $5, $6)', data).then(() => {
    return {
      accessToken: token.accessToken,
      accessTokenExpiresAt: token.accessTokenExpiresAt,
      refreshToken: token.refreshToken,
      refreshTokenExpiresAt: token.refreshTokenExpiresAt,
      client: client,
      user: user
    };
  }).catch((err) => {
    debug('saveToken - err', err);
    return false;
  });
};

module.exports.revokeToken = function *(token) {
  debug('revokeToken', token);
  return pg.none('DELETE FROM oauth.tokens WHERE refresh_token = $1', [token.refreshToken]).then(() => {
    return true;
  }).catch((err) => {
    debug('saveToken - err', err);
    return false;
  });
};